package games.negative.framework.discord.command.exception;

@Deprecated
public class InvalidCommandInformationException extends RuntimeException {

    public InvalidCommandInformationException(String message) {
        super(message);
    }
}
